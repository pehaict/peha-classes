<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$oPassword = new passwordgenerator();

$oPassword->setLength(15)
    ->setCaps(true)
    ->setCapsChange(5)
    ->setStrength(5);

if(DEVELOPMENT) {
    echo 'regel: ' . __LINE__ . ' in file ' . __FILE__;
    printf('<pre>%s</pre>', print_r($oPassword, 1));
}

$p =  $oPassword->generatePassword();

    var_dump($p);
