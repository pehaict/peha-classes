<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$pass = filter_input(INPUT_POST, 'pass');
$l = filter_input(INPUT_POST, 'length');

$checker = new passwordstrength();
$checker->setMinLength($l ? : 8);
$checker->setRequireCaps(filter_input(INPUT_POST, 'caps') ? true : false);
$checker->setRequireLetters(filter_input(INPUT_POST, 'letter') ? true : false);
$checker->setRequireNumber(filter_input(INPUT_POST, 'num') ? true : false);
$checker->setRequireOther(filter_input(INPUT_POST, 'other') ? true : false);
?>
<html>
    <body>
        <h1>Password strength checker</h1>
        <?php
        if($pass) {
            if($checker->checkStrength($pass)) {
                echo 'password ' . $pass . ' does meet requirements';
            }
            else {
                echo 'password ' . $pass . ' does NOT meet requirements';
            }
        }
        ?>

        <br>
        <br>
        <br>
        <form method="post">

            <h2>Requirements</h2>
            <ul>
                <li><input type="checkbox" name="caps" value="1" <?= filter_input(INPUT_POST, 'caps') ? 'checked="checked"':''?>> Caps</li>
                <li><input type="checkbox" name="letter" value="1" <?= filter_input(INPUT_POST, 'letter') ? 'checked="checked"':''?>> letter</li>
                <li><input type="checkbox" name="num" value="1" <?= filter_input(INPUT_POST, 'num') ? 'checked="checked"':''?>> number</li>
                <li><input type="checkbox" name="other" value="1" <?= filter_input(INPUT_POST, 'other') ? 'checked="checked"':''?>> other</li>
            </ul>
            Minimum length: <input type="number" name="length" value="<?= $l ?>"><br>
            <br>

            <input name="pass" value="<?= htmlspecialchars($pass) ?>">




            <button type="submit">ok</button>


        </form>

    </body>
</html>




