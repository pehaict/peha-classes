<?php
const DEVELOPMENT = true;

if($_SERVER['REQUEST_URI'] == '/') {
    header('Location: /index.php');
    exit();
}
if($_SERVER['REQUEST_URI'] != $_SERVER['SCRIPT_NAME'] )  {

    include './config/autoloader.php';
    
    
    require_once '.' . $_SERVER['SCRIPT_URL'];
    exit();
}


$aFiles = glob('./*php');
?><html>
    <head>
        <title>Classes tester</title>
    </head>
    <body>
        <h1>Classes ontwikkeling</h1>
        <p>Volgende classes kunnen getest worden:</p>
        <ul>
            <?php foreach($aFiles as $file) : ?>
                <?php if($file != './index.php') : ?>
            <li><a href="<?= $file ?>"><?= $file ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </body>
</html>