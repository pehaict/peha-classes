# README #

This repository contains some PHP classes for general use in any application.

It was created as I needed some of these classes in different projects, and in all projects I made some modifications to them. 
And so I ended up with a lot of differences in basically the same script.

So to keep track of the changes, this repo was created.

### What is this repository for? ###

* Group of PHP classes for general use in projects.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### What classes are available? ###
* Pagination
* Password (generating password)
* Password strength checker


### How do I get set up? ###

In general there will be a file named foo.php in the document root of the repository.
It will contain a test to access the actual class placed in the file ./classes/foo.class.php

Access through index.php. A .htaccess file with some rewrite rules will grant access to the test scripts.

* Summary of set up
* Configuration
** only a autoloader file
* Dependencies
** PHP 7
* Database configuration
** none
* How to run tests
** TODO
* Deployment instructions
** TODO


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* classes.git@peha-ict.nl Ivo Peters