<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$totaalitems = 180;

$currentPage = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);

$pagination = new pagination();

$pagination->set_current_page($currentPage)
    ->set_total_items($totaalitems);
?><html>
    <head>
        <style>
            ul {
                margin-left: 30px;
                list-style-type: none;
                
            }
            li {
                display: inline;
            }
            a.active {
                font-weight: bold;
            }
            a {
                margin-right: 10px;
            }
        </style>
        <title>title</title>
    </head>
    <body>
        <ul>
<?php echo $pagination->get_pagination(); ?>
            </ul>
    </body>
</html>






