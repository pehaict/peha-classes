<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class autoloader
{
    const classpath = '/classes/';
    
    public static function classes(string $sClass)
    {
        $filename = '.'. self::classpath . strtolower($sClass) . '.class.php';
        if(is_file($filename)) {
            require_once $filename;
        }
    }
}

spl_autoload_register('autoloader::classes');