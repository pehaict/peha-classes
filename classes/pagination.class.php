<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pagination
 *
 * @author peha
 */
class pagination
{

    protected $totalitems;
    protected $iNumberPerPage = 10;
    protected $totalpages;
    protected $current;
    protected $lasttoshow;
    protected $firsttoshow;
    protected $bShowAlways = false;
    protected $iCrumbs = 5;
    protected $urlKey = 'page';
    protected $aUrlParameters;
    protected $sUrl;
    protected $prefix = '<li>';
    protected $postfix = '</li>';
    
    protected $bShowNextPrev = true;
    protected $bShowLastFirst = true;
    
    protected $bNumericalFirstLast = false;


    protected $sFirstLabel = '&laquo;- First';
    protected $sPreviousLabel = '&laquo; Prev';
    protected $sNextLabel = 'Next &raquo;';
    protected $sLastLabel = 'Last -&raquo;';
    
    protected $classNumericalElements = 'number';
    protected $classTextElements = '';





    public function __construct(int $currentPage=null, int $totalElements=null)
    {
        $this->aUrlParameters = $_GET;
        if($currentPage) {
            $this->set_current_page($currentPage);
        }
        if($totalElements) {
            $this->set_total_items($totalElements);
        }
    }

    /**
     * 
     * @param int $pagenr
     * @return \pagination
     */
    public function set_current_page(int $pagenr)
    {
        $this->current = $pagenr;
        return $this;
    }

    
    /**
     * 
     * @param string $tag
     * @return \pagination
     */
    public function set_prefix_link(string $tag)
    {
        $this->prefix = $tag;
        return $this;
    }
    
    /**
     * 
     * @param string $tag
     * @return \pagination
     */
    public function set_postfix_link(string $tag)
    {
        $this->postfix = $tag;
        return $this;
    }
    
    /**
     * 
     * @param string $text
     * @return \pagination
     */
    public function set_first_label(string $text)
    {
        $this->sFirstLabel = $text;
        return $this;
    }

    /**
     * 
     * @param string $text
     * @return \pagination
     */
    public function set_last_label(string $text)
    {
        $this->sLastLabel = $text;
        return $this;
    }

    /**
     * 
     * @param string $text
     * @return \pagination
     */
    public function set_previous_label(string $text)
    {
        $this->sPreviousLabel = $text;
        return $this;
    }

    /**
     * 
     * @param string $text
     * @return \pagination
     */
    public function set_next_label(string $text)
    {
        $this->sNextLabel = $text;
        return $this;
    }

    /**
     * 
     * @param int $nrofitems
     * @return \pagination
     */
    public function set_total_items(int $nrofitems)
    {
        $this->totalitems = $nrofitems;
        return $this;
    }

    /**
     * 
     * @param int $nr
     * @return \pagination
     */
    public function set_nr_per_page(int $nr)
    {
        $this->iNumberPerPage = $nr;
        return $this;
    }

    /**
     * 
     * @param bool $b
     * @return \pagination
     */
    public function always_show(bool $b = true)
    {
        $this->bShowAlways = $b;
        return $this;
    }

    /**
     * 
     * @param int $nr
     * @return \pagination
     */
    public function set_nr_of_crumbs(int $nr)
    {
        $this->iCrumbs = $nr;
        return $this;
    }

    /**
     * Get list of links to show on page
     * 
     * @return string 
     */
    public function get_pagination()
    {
        $this->totalpages = ceil($this->totalitems / $this->iNumberPerPage);

        $this->calc_crumbs_to_show();



        $return = '';

        if($this->bShowLastFirst) {
            $url = $this->current > 1 ? $this->create_url(1) : '#';
            $return .= $this->create_link($url, $this->sFirstLabel);
        }

        if($this->bShowNextPrev) {
            $url = $this->current > 1 ? $this->create_url($this->current - 1) : '#';
            $return .= $this->create_link($url, $this->sPreviousLabel);
        }

        for($i = $this->firsttoshow; $i <= $this->lasttoshow; $i++) {
            $url = $this->create_url($i);

            $class = ($this->current == $i) ? 'active' : '';

            $return .= $this->create_link($url, $i, $class);
        }
        if($this->bShowNextPrev) {
            $url = $this->current < $this->totalpages ? $this->create_url($this->current + 1) : '#';
            $return .= $this->create_link($url, $this->sNextLabel);
        }

        if($this->bShowLastFirst) {
            $url = $this->current < $this->totalpages ? $this->create_url($this->totalpages) : '#';
            $return .= $this->create_link($url, $this->sLastLabel);
        }

        return $return;
    }

    /**
     * 
     * @param string $url
     * @param int $nr
     * @param string $class
     * @return string
     */
    protected function create_link(string $url, string $label, string $class = '')
    {
        return $this->prefix . '<a href="' . $url . '" class="' . $class . '">' . $label . '</a>' . $this->postfix;
    }

    /**
     * 
     * @return type
     */
    protected function calc_crumbs_to_show()
    {
        $this->firsttoshow = 1;
        $this->lasttoshow = $this->totalpages;

        if($this->lasttoshow < $this->iCrumbs) {
            return;
        }
        else {
            if($this->current > floor($this->iCrumbs / 2)) {
                $this->firsttoshow = $this->current - floor($this->iCrumbs / 2);
            }

            if($this->totalpages >= $this->firsttoshow + $this->iCrumbs) {
                $this->lasttoshow = $this->firsttoshow + $this->iCrumbs - 1;
            }

            if($this->lasttoshow - $this->firsttoshow < $this->iCrumbs - 1) {
                $this->firsttoshow = $this->lasttoshow - $this->iCrumbs + 1;
            }
        }
    }

    /**
     * 
     * @param int $iPage
     * @return string
     */
    protected function create_url(int $iPage)
    {
        if(!$this->sUrl) {
            unset($this->aUrlParameters[$this->urlKey]);

            $this->aUrlParameters[$this->urlKey] = '';
            $this->sUrl = $_SERVER['SCRIPT_URL'] . '?' . http_build_query($this->aUrlParameters);
        }

        return $this->sUrl . $iPage;
    }

}
