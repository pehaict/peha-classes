<?php
/*
The MIT License (MIT)

Copyright (c) 2015 passwordgenerator

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/**
 * Create a password of certain strength and length
 * @author peha
 */
class passwordgenerator
{

    /**
     * Length of password to generate
     * @var int
     */
    protected $length = 8;
    
    
    /**
     * Strength of the password to create
     * range from 1 to 5
     * @var int
     */
    protected $strength = 3;
    protected $useCaps = true;
    protected $capsChange = 2;
    protected $sets = 'vvcccnnsu';
    
    protected $vowels = 'aeuy';
    protected $co = 'bcdfghjkmnpqrstvwxz';
    protected $numbers = '23456789';
    protected $specials = '!@#%&*()-=+?';
    protected $unusuals = '.,:;{}[]/';
    protected $setsToUse;

    /**
     * 
     * @return string
     */
    public function generatePassword()
    {
        $this->setSets();
        $generatedPassword = '';
        for($i = 0; $i < $this->length; $i++) {
            $set = $this->setsToUse[rand(0, count($this->setsToUse)-1)];

            $charToAdd = $set{rand(0, strlen($set) - 1)};
            if($this->useCaps && (rand(0, 9) % $this->capsChange) == 0) {
                $generatedPassword .= strtoupper($charToAdd);
            }
            else {
                $generatedPassword .= $charToAdd;
            }
        }

        return $generatedPassword;
    }

    /**
     * 
     * @param int $length
     * @return \password
     */
    public function setLength(int $length)
    {
        $this->length = max(1,$length);
        return $this;
    }

    /**
     * 
     * @param int $strength
     * @return \password
     */
    public function setStrength(int $strength)
    {
        $this->strength = min(max(0,$strength),5);
        return $this;
    }

    /**
     * 
     * @param bool $caps
     * @return \password
     */
    public function setCaps(bool $caps)
    {
        $this->useCaps = $caps;
        return $this;
    }

    /**
     * 
     * @param int $capsChange
     * @return \password
     */
    public function setCapsChange(int $capsChange)
    {
        $this->capsChange = $capsChange;
        return $this;
    }

    /**
     * Fill array containing the sets of chars to use in the password
     */
    protected function setSets()
    {
        $sets = substr($this->sets, 0, $this->strength + 4);

        for($i = 0; $i < strlen($sets); $i++) {
            switch(substr($sets, $i, 1)) {
                case 'v':
                default:
                    $this->setsToUse[] = $this->vowels;
                    break;
                case 'c':
                    $this->setsToUse[] = $this->co;
                    break;
                case 'n':
                    $this->setsToUse[] = $this->numbers;
                    break;
                case 's':
                    $this->setsToUse[] = $this->specials;
                    break;
                case 'u':
                    $this->setsToUse[] = $this->unusuals;
                    break;
            }
        }
    }

}
