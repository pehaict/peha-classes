<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of passwordstrength
 * check if given password meets the minimum requirement
 * 
 * either it needs to match a certain length en have characters from defined sets
 * or it needs to have characters from at least the given number of sets.
 * 
 * length is always checked.
 *
 * @author peha
 */
class passwordstrength
{

    protected $minLenth = 8;
    protected $requireCaps = true;
    protected $requireLetters = true;
    protected $requireNumber = false;
    protected $requireOther = false;
    protected $requireNumberOfSets = 0;

    function __construct()
    {
        
    }

    /**
     * 
     * @param string $password
     * @return boolean
     */
    public function checkStrength(string $password)
    {
        if($this->requireNumberOfSets) {
            return $this->checkStrengthNumberOfSets($password);
        }
        else {
            return $this->checkStrengthHardCoded($password);
        }

        return $return;
    }

    /**
     * 
     * @param string $password
     * @return boolean
     */
    protected function checkStrengthHardCoded(string $password)
    {
        $regex = '#^\S*';
        // length:
        $regex .= sprintf('(?=\S{%d,})', $this->minLenth);

        if($this->requireCaps) {
            $regex .= '(?=\S*[A-Z])';
        }
        if($this->requireLetters) {
            $regex .= '(?=\S*[a-z])';
        }
        if($this->requireNumber) {
            $regex .= '(?=\S*[\d])';
        }
        if($this->requireOther) {
            $regex .= '(?=\S*[\W])';
        }

        $regex .= '\S*$#';

        return preg_match($regex, $password);
    }

    /**
     * 
     * @param string $password
     * @return boolean
     */
    protected function checkStrengthNumberOfSets(string $password)
    {
        $return = false;

        if(mb_strlen($password) > $this->minLenth) {

            $count = 0;
            // caps
            if(preg_match('#[A-Z]#', $pass)) {
                $count++;
            }
            if(preg_match('#[a-z]#', $pass)) {
                $count++;
            }
            if(preg_match('#[\d]#', $pass)) {
                $count++;
            }
            if(preg_match('#[\W]#', $pass)) {
                $count++;
            }

            $return = $count >= $this->requireNumberOfSets;
        }
        return $return;
    }

    /**
     * 
     * @param int $length
     * @return \passwordstrength
     */
    public function setMinLength(int $length)
    {
        $this->minLenth = $length;
        return $this;
    }

    /**
     * 
     * @param bool $require
     * @return \passwordstrength
     */
    public function setRequireCaps(bool $require=true)
    {
        $this->requireCaps = $require;
        return $this;
    }

    /**
     * 
     * @param bool $require
     * @return \passwordstrength
     */
    public function setRequireLetters(bool $require=true)
    {
        $this->requireLetters = $require;
        return $this;
    }

    /**
     * 
     * @param bool $require
     * @return \passwordstrength
     */
    public function setRequireNumber(bool $require=true)
    {
        $this->requireNumber = $require;
        return $this;
    }

    /**
     * 
     * @param bool $require
     * @return \passwordstrength
     */
    public function setRequireOther(bool $require=true)
    {
        $this->requireOther = $require;
        return $this;
    }

}
